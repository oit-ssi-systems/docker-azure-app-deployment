FROM registry.gitlab.com/gitlab-org/terraform-images/stable:latest AS gitlab-terraform
FROM ubuntu:20.04

ENV VAULT_ADDR https://vault-systems.oit.duke.edu
ENV PIP_EXTRA_INDEX_URL https://piepie.oit.duke.edu/simple/
ENV DEBIAN_FRONTEND 'noninteractive'
ENV HELMFILE_VERSION '0.139.5'
ENV KUBECTLVERSION 'v1.19.7'
ENV TERRAFORM_AKA_VERSION '0.1.1'

RUN mkdir /code
COPY . /code/
WORKDIR /code

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

COPY --from=gitlab-terraform /usr/bin/gitlab-terraform /usr/bin/

# hadolint ignore=DL3008,DL3005
RUN apt-get -y update && \
    apt-get -y dist-upgrade && \
    apt-get -y install --no-install-recommends \
      curl ca-certificates gnupg2 git software-properties-common \
      jq unzip && \
    curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
    apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
    chmod 700 get_helm.sh && \
    ./get_helm.sh  && \
    rm -f get_helm.sh && \
    apt-get -y update && \
    apt-get -y install --no-install-recommends terraform && \
    rm -rf /var/lib/apt/lists/* && \
    curl -fsSL -o "vault.zip" https://releases.hashicorp.com/vault/1.6.2/vault_1.6.2_linux_amd64.zip && \
    unzip vault.zip && \
    mv vault /usr/bin/ && \
    rm -f vault.zip && \
    curl -fsSL "https://github.com/roboll/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_linux_amd64" --output helmfile_linux_amd64 && \
    mv helmfile_linux_amd64 /usr/local/bin/helmfile && \
    chmod 755 /usr/local/bin/helmfile && \
    curl -sL https://aka.ms/InstallAzureCLIDeb | bash && \
    az aks install-cli && \
    curl -fsSL -o terraform-aka.zip https://gitlab.oit.duke.edu/devil-ops/terraform-provider-aka/uploads/b323c73b82b269a398a39fab3e296a25/terraform-provider-aka_0.1.1_linux_amd64.zip && \
    mkdir -p ~/.terraform.d/plugins/gitlab.oit.duke.edu/devil-ops/aka/${TERRAFORM_AKA_VERSION}/linux_amd64/ && \
    unzip -o terraform-aka.zip && \
    mv terraform-provider-aka_v${TERRAFORM_AKA_VERSION} ~/.terraform.d/plugins/gitlab.oit.duke.edu/devil-ops/aka/${TERRAFORM_AKA_VERSION}/linux_amd64/terraform-provider-aka && \
    rm -f terraform-aka.zip
